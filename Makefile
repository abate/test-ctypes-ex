
all:
	dune build @install

clean:
	dune clean
	make -C tests clean
	find -name dune-project -exec rm {} \;

test: all
	make -C tests
