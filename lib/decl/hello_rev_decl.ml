open Ctypes
(*open Foreign*)

module Hello = struct
  let hello s = Printf.sprintf "Hello %s" s

  type t = { msg : string }

  let init msg = { msg }
  let hello2 { msg } = hello msg

end

module Rev_bindings (I : Cstubs_inverted.INTERNAL) = struct

  (* Add the binding here for the function Hello.hello and the function hello2 
   * defined above *)

end
